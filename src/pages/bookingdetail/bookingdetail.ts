import { Component } from '@angular/core';
import {  NavController, NavParams, Platform,ToastController,AlertController } from 'ionic-angular';
import { TabPage } from '../tab/tab';
import 'rxjs/add/operator/map';
import { Providers } from '../../providers/string/string';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Geolocation  } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import * as $ from "jquery";

// @IonicPage()
@Component({
  selector: 'page-bookingdetail',
  templateUrl: 'bookingdetail.html',
})
export class BookingdetailPage {
 fetchUser: string;
 OrderID: string;
 UserID:any;
 UserData: Array<any>;
 UserOrder: Array<any>;
 userlocation:any;
 order_ID:any;
 DelOp:any;
 OrderQuant:any;
 value:any;
 hidden:boolean=false;
 DelOpt:any;
 Where:any;
 Locate:any;
 URLStringData: string = this.URLStr.URL + 'getID.php';
 URLStringShop: string = this.URLStr.URL + 'item.php';
 URLPhotos: string = this.URLStr.URL + 'photos/items/';
 ExStand:any;
  constructor(
    private nativeGeocoder: NativeGeocoder,
    private geolocation: Geolocation,
    public platForm: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public URLStr: Providers,
    private http: Http,
    public toast: ToastController,
    public locationAccuracy: LocationAccuracy,
    private alertCtrl: AlertController) {
    platForm.registerBackButtonAction(() => {
      this.navCtrl.popTo(TabPage);
    }), 3000;
    	this.fetchUser = navParams.get('fetchUser');
      this.OrderID = navParams.get('OrderID');
      this.DelOpt = navParams.get('DelOpt');
      if(this.DelOpt == "1"){
        this.hidden = true;
      } else if(this.DelOpt == "3") {
        this.hidden = false;
      }
        this.LoadData();
  }
  getUserLocation(){
    let option = {
      timeout: 10000,
      enableHighAccuracy: true,
      maximumAge: 3600
    };
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () =>
          this.platForm.ready().then(()=>{
            this.geolocation.getCurrentPosition(option).then((data) =>{
              this.nativeGeocoder.reverseGeocode(8.353048, 124.814248)
              .then((result: NativeGeocoderReverseResult) => {
                 // alert(JSON.stringify(result));
              }, error => {
                console.log(error)
              });
       }).catch((err)=>{
           console.log("Error", err);
         });
      }),error => console.log('Error requesting location permissions', error)
        );
      }
    });
    }
  LoadData() {
    this.UserData = [];
    this.UserOrder = [];
    var headers = new Headers();
    headers.append("Accept",'application/json');
    headers.append('Content-Type','application/json');
    let options = new RequestOptions({
      headers: headers
    });
    let data = {
      Username: this.fetchUser,
      OrderID: this.OrderID,
    };
    this.http.post(this.URLStringShop, data, options).map(res => res.json()).subscribe(res => {
        res.forEach((data) => {

          var text = this.URLPhotos + data.item_img;
              this.UserOrder.push({
                name: data.item_name,
                descp: data.item_description,
                Liter: data.item_liters,
                avatar: text,
                value: data.value,
            });
              this.value = data.value;
        });
    });
    this.http.post(this.URLStringData, data, options).map(res => res.json()).subscribe(res => {
        res.forEach((data) => {
          var totalFee;
          let addfee;
          if(data.Delivery_ID == 3){
             addfee = 10;
             totalFee = 25 * data.Order_Quantity + addfee;
             this.ExStand = "Express Delivery";
          } else {
            addfee = 5;
             totalFee = 25 * data.Order_Quantity + addfee;
             this.ExStand = "Standard Delivery";
          }
            this.UserData.push({
            odrQuan: data.Order_Quantity,
            DelOp: this.DelOp,
            AdditionalFee: addfee,
            Total: totalFee,
          });
        });
    });
  }
}
