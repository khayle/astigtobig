import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ShopPage } from '../shop/shop';
import 'rxjs/add/operator/map';
import { Providers } from '../../providers/string/string';
 // IonicPage,
// import { Http, Headers, RequestOptions } from '@angular/http';

// @IonicPage()
@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})
export class TransactionPage {

  constructor(public platForm: Platform, public navCtrl: NavController, public navParams: NavParams ,public URLStr: Providers) {
    platForm.registerBackButtonAction(() => {
      this.navCtrl.popTo(ShopPage);
    }), 3000;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionPage');
  }

}
