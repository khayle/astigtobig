import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { Providers } from '../../providers/string/string';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { TabPage } from '../tab/tab';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  User_username: any;
  User_password: any;
  loading: any;
  PasswordType: string = 'password';
  PasswordIcon: string = 'md-eye-off';
  URLString: string = this.URLStr.URL + 'login.php';
  show = true;
  data: string;
  constructor(
  public URLStr: Providers,
  public http: Http,
  public toastCtrl: ToastController,
  public loadingCtrl: LoadingController,
  public navCtrl: NavController) {
  }
  showHide() {
		if (this.show) {
		    this.PasswordType = 'text';
		    this.PasswordIcon = 'md-eye'
		    this.show = false;
		} else {
		    this.PasswordType = 'password';
		    this.PasswordIcon = 'md-eye-off'
		    this.show = true;
		}
	}
  LoginAccount(){
    if (this.User_username == null) {
      const toast = this.toastCtrl.create({
      message: 'Username Field is Empty!',
      duration: 2000
      });
      toast.present();
    } else if (this.User_password == null) {
      const toast = this.toastCtrl.create({
      message: 'Password Field is Empty!',
      duration: 2000
      });
      toast.present();
    } else {
      var headers = new Headers();
      headers.append("Accept",'application/json');
      headers.append('Content-Type','application/json');
      let options = new RequestOptions({
        headers: headers
      });
      let data = {
        Username: this.User_username,
        Password: this.User_password,

      };
      this.loading = this.loadingCtrl.create({
        content: 'Logging In....',
      });
      this.loading.present().then(() => {
      this.http.post(this.URLString, data, options).map(res => res.json()).subscribe(res => {
        if (res == "1") {
          this.loading.dismiss();
          this.navCtrl.push(TabPage, {User: this.User_username});
          this.User_username = '';
          this.User_password = '';
        } else {
          const toast = this.toastCtrl.create({
            message: 'Incorrect Username or Password!',
            duration: 2000
          });
          toast.present();
          this.loading.dismiss();
          this.User_username = '';
          this.User_password = '';
        }
      });
      });
    }
  }
}
