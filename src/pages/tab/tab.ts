import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ShopPage } from '../shop/shop';
import { TransactionPage } from '../transaction/transaction';
import { ProfilePage } from '../profile/profile';

// @IonicPage()
@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {
  tab1Root = ShopPage;
  tab2Root = TransactionPage;
  tab3Root = ProfilePage;
  User:any;
  constructor(public navParams: NavParams) {
      this.User = { fetchUser: navParams.get('User') };
  }
}
