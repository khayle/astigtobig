import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,Platform,LoadingController,ToastController,App  } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Providers } from '../../providers/string/string';
import { Http, Headers, RequestOptions } from '@angular/http';
import { BookingdetailPage } from '../bookingdetail/bookingdetail';
// @IonicPage()
@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html',
})
export class ShopPage {
 currentNumber:number = 0;
 fetchUser: string;
 loading: any;
 deliverOption: any;
 ExprDel:boolean = false;
 PickUp:boolean = false;
 NormalDel:boolean = false;
 shopinfo = [];
 URLString: string = this.URLStr.URL + 'order.php';
 URLStringShop: string = this.URLStr.URL + 'shop.php';
 URLPhotos: string = this.URLStr.URL + 'photos/items/';
 priceforStand:any;
 priceforExp:any;
 PriceDesc:any;
 NoteSta:any;
 NoteEx:any;
 buttonDisabled:boolean = false;
 chckBox:boolean = false;
  constructor(
  public loadingCtrl: LoadingController,
  public platForm: Platform,
  public URLStr: Providers,
  private http: Http,
  public navCtrl: NavController,
  public navParams: NavParams,
  public alertCtrl: AlertController,
  public toastCtrl: ToastController,
  private app: App
)
{
  	this.fetchUser = navParams.get('fetchUser');
      this.LoadItemsShop();

      let cD = new Date();
      let currentTime = cD.getHours();

    if (currentTime > 21 && currentTime == 24){
      this.buttonDisabled = true;
    } else if (currentTime > 0 && currentTime < 7){
      this.buttonDisabled = true;
    } else if (currentTime > 17 && currentTime < 21){
      this.chckBox = true;
    } else {
        this.buttonDisabled = false;
    }
}
currentNumbers(){
  this.priceforExp = this.currentNumber * 35;
  this.priceforStand = this.currentNumber * 30;
  this.PriceDesc = " Total price: ";
}
LoadItemsShop(){
  var headers = new Headers();
  headers.append("Accept",'application/json');
  headers.append('Content-Type','application/json');
  let options = new RequestOptions({
    headers: headers
  });
  let data = {
    id: 1,
  };
  this.http.post(this.URLStringShop, data, options).map(res => res.json()).subscribe(res => {
          var num = Number(res.value).toLocaleString('en');
          res.forEach((data) => {
                this.shopinfo.push({
                  id: data.id,
                  name: data.item_name,
                  descp: data.item_description,
                  Liter: data.item_liters,
                  avatar: this.URLPhotos + data.item_img,
                  value: num,
              });
          });
  });
}
   increment () {
     if (this.currentNumber == 99) {
        this.currentNumber = 99;
     } else if (this.currentNumber == 0 || this.currentNumber > 0) {
         this.currentNumber++;
         this.priceforExp = (this.currentNumber * 25) + 10;
         this.priceforStand = (this.currentNumber * 25) + 5;
         this.PriceDesc = " Total price: ";
     }
  }
   decrement () {
     let cD = new Date();
     let currentTime = cD.getHours();
     if(this.currentNumber > 0){
         this.currentNumber--;
         this.priceforExp = (this.priceforExp - 35) + 10;
         this.priceforStand = (this.priceforStand - 30) + 5;
         this.PriceDesc = " Total price: ";
         if(this.currentNumber == 0 ){
           this.priceforExp = "";
           this.priceforStand = "";
           this.PriceDesc = "";
         }
     }else if (this.currentNumber == 0 ){
        this.currentNumber = 0;
     }
  }
  expresdel(e:any){
  if(e.checked){
    this.ExprDel = true;
    this.NormalDel = false;
    let alert = this.alertCtrl.create({
     subTitle: 'Guaranteed Express Deliviery within 30 Mins<br><br>Additional Charge 5 PHP per bottle',
     buttons: ['Ok']
   });
   alert.present();
 } else {
     this.ExprDel = false;
 }
}
deliv(d:any){
  if(d.checked){
    this.NormalDel = true;
    this.ExprDel = false;
  } else {
    this.NormalDel = false;
  }
}
  UserOrder () {
    let cD = new Date();
    let currentTime = cD.getHours();
    if(this.currentNumber > 0){

    if (this.ExprDel){
      this.deliverOption = 2;
    }  else if (this.NormalDel) {
      this.deliverOption = 1;
    }

   if(currentTime > 7 && currentTime < 13){
     this.NoteSta = "Afternon";
     this.NoteEx = "Morning";
   } else if (currentTime > 13 && currentTime < 18) {
     this.NoteSta = "Tomorrow";
     this.NoteEx = "Afternoon";
   } else if (currentTime > 17 && currentTime < 21) {
     this.NoteSta = "Close";
     this.NoteEx = "Today";
   }
   if(this.deliverOption == 1){
     let alert = this.alertCtrl.create({
      subTitle: 'Your order will be delivered within this '+this.NoteSta,
      buttons: ['Ok']
    });
    alert.present();
    this.OrderNow();
  } else {
    let alert = this.alertCtrl.create({
     subTitle: 'Your order will be delivered within this '+this.NoteEx,
     buttons: ['Ok']
   });
   alert.present();
  }
  } else {
    const toast = this.toastCtrl.create({
      message: 'Please Enter valid quantity!',
      duration: 2000
    });
    toast.present();
    }
}

OrderNow(){
  let cD = new Date();
  let dT = cD.getFullYear()+"-"+(cD.getMonth()+1)+"-"+cD.getDate()+" "+cD.getHours()+":"+cD.getMinutes()+":"+cD.getSeconds();
  var headers = new Headers();
  headers.append("Accept",'application/json');
  headers.append('Content-Type','application/json');
  let options = new RequestOptions({
    headers: headers
  });
  let data = {
    Delivery: this.deliverOption,
    Username: this.fetchUser,
    OrdrQuantity: this.currentNumber,
    ItemOrder: 1,
    Datee: dT,
  };
  this.loading = this.loadingCtrl.create({
    content: 'Processing ..',
  });
  this.loading.present().then(() => {
  this.http.post(this.URLString, data, options).map(res => res.json()).subscribe(res => {
    if (res > 1) {
      this.loading.dismiss();
      this.app.getRootNav().push(BookingdetailPage, {fetchUser:this.fetchUser,OrderID:res,DelOpt:this.deliverOption});
      this.deliverOption = 0;
      this.NormalDel = false;
      this.ExprDel = false;
      this.PickUp = false;
      this.currentNumber = 0;
      this.PriceDesc = null;
      this.priceforExp = "";
      this.priceforStand = "";
    } else {
      const toast = this.toastCtrl.create({
        message: 'Error!',
        duration: 2000
      });
      toast.present();
      this.loading.dismiss();
    }
  });
  });
}
}
