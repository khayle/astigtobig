import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ShopPage } from '../pages/shop/shop';
import { TransactionPage } from '../pages/transaction/transaction';
import { ProfilePage } from '../pages/profile/profile';
import { TabPage } from '../pages/tab/tab';
import { SettingsPage } from '../pages/settings/settings';
import { BookingdetailPage } from '../pages/bookingdetail/bookingdetail';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { HttpModule } from '@angular/http';
import { Providers } from '../providers/string/string';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ShopPage,
    TransactionPage,
    ProfilePage,
    SettingsPage,
    TabPage,
    BookingdetailPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ShopPage,
    TransactionPage,
    ProfilePage,
    SettingsPage,
    TabPage,
    BookingdetailPage
  ],
  providers: [
    LocationAccuracy,
    Providers,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    GeocoderProvider,
    NativeGeocoder,

  ]
})
export class AppModule {}
